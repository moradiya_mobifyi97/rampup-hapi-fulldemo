const Path = require('path');

module.exports = {
    server: {
        port: 8000,
        routes:{
            files:{
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    },
    register: {
        plugins: [
            {
                plugin:require('inert')
            },
            {
                plugin:'vision',
                options:{
                    engines:{
                        html:require('handlebars')
                    },
                    path:__dirname,
                    layout:true,
                    layoutPath:'tempaltes/layouts'
                }
            },
            {
                plugin:require('./auth')
            },
            {
                plugin:require('./home')
            },
            {
                plugin:require('./manager'),
                routes:{
                    prefix:'/manager'
                }
            },
            {
                plugin: require('./employee'),
                routes:{
                    prefix:'/employee'
                }
            }
        ]
    }
};