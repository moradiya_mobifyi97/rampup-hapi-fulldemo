const users = {
    john: {
        username: 'john',
        password: 'john',   // 'secret'
        name: 'John Doe',
        id: '1234'
    },
    admin: {
        username: 'admin',
        password: 'admin',   // 'secret'
        name: 'admin',
        id: '12345'
    }
};

module.exports.validate = async (request, username, password, h) => {
    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = password === user.password;
    const credentials = { id: user.id, username: user.username };

    return { isValid, credentials };
};