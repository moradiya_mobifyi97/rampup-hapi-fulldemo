const service = require('./servic')
exports.register = async function (server){
    await server.register(require('bell'));
    await server.register(require('hapi-auth-basic'));

    server.auth.strategy('simple', 'basic', { validate:service.validate });
    
}
exports.pkg = {
    name:"auth"
}